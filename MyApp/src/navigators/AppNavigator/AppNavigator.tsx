import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from '../../screens/HomeScreen';
import CreateTodoListScreen from '../../screens/CreateTodoListScreen';
import {Linking} from '../../config/routing-config';
import ViewTodoListcreen from '../../screens/ViewTodoListcreen';

const Stack = createNativeStackNavigator();

const AppNavigator = () => {
  return (
    <NavigationContainer linking={Linking}>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CreateTodoList"
          component={CreateTodoListScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ViewTodoList"
          component={ViewTodoListcreen}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
