import React, {useState} from 'react';
import CurrentColor from './CurrentColorContext';

/**
 * Context Hook holding farmer instance during farmer form process
 * @param props
 */
const CurrentColorProvider = ({children}: {children: any}) => {
  const state = {name: '', color: ''};
  const [currentColor, setCurrentColor] = useState(state);
  const options = {currentColor, setCurrentColor};

  return (
    <CurrentColor.Provider value={options}>{children}</CurrentColor.Provider>
  );
};

export default CurrentColorProvider;
