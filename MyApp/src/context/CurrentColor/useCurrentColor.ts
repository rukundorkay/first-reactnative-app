import {useContext} from 'react';

import CurrentColor from './CurrentColorContext';

/**
 * Context Hook holding farmer instance during farmer form process
 */
const useCurrentColor = () => {
  const context = useContext(CurrentColor);
  if (context === undefined) {
    throw new Error('useCurrentColor must be used within an CurrentColorContext');
  }

  return context;
};

export default useCurrentColor;
