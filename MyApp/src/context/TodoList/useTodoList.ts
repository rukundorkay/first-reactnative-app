import {useContext} from 'react';

import TodoList from './TodoList';

/**
 * Context Hook holding farmer instance during farmer form process
 */
const useTodoList = () => {
  const context = useContext(TodoList);
  if (context === undefined) {
    throw new Error('useTodoList must be used within an TodoListContext');
  }

  return context;
};

export default useTodoList;
