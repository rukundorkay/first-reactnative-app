import React, {useState} from 'react';
import TodoList from './TodoList';
import {color} from '../../styles';
/**
 * Context Hook holding farmer instance during farmer form process
 * @param props
 */
const TodoListProvider = ({children}: {children: any}) => {
  const state = [
    {
      name: 'Firebase',
      ikureri: color.PURPLE,
      todos: ['createAcount'],
    },
    {
      name: 'Database',
      ikureri: color.BLUE,
      todos: ['task 1', 'task 2', 'task 3'],
    },
  ];
  const [todoList, setTodoList] = useState(state);
  const options = {todoList, setTodoList};

  return <TodoList.Provider value={options}>{children}</TodoList.Provider>;
};

export default TodoListProvider;
