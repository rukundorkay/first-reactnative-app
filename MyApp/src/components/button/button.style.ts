import {StyleSheet} from 'react-native';
import { color } from '../../styles';
export const styles =(btncolor:string)=>StyleSheet.create({
  
  buttonContent:{
    color:color.WHITE
  },
    
  button:{
    borderWidth: 0.3,
    borderRadius: 3,
    width: 300,
    height: 40,
    backgroundColor:btncolor,
    alignItems:'center',
    paddingTop:7,
    borderColor:btncolor


  },



})