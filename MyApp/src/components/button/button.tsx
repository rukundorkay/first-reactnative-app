import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {styles} from './button.style';
const ButtonView: React.FC<any> = ({color, submit}) => (
  <TouchableOpacity
    style={styles(color).button}
    onPress={() => submit.handleSubmit()}>
    <Text style={styles('').buttonContent}>Create!</Text>
  </TouchableOpacity>
);
export default ButtonView;
