import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {styles} from './todolist.style';
import {useCurrentColor} from '../../context/CurrentColor';

const TodoList: React.FC<any> = ({bgcolor, name, navigator}) => {
  const {setCurrentColor}: any = useCurrentColor();
  const handleView = (name: string, bgcolor: string, navigator: any) => {
    setCurrentColor({name: name, color: bgcolor});
    navigator.navigate('ViewTodoList');
  };
  return (
    <TouchableOpacity
      style={styles(bgcolor).todolist}
      onPress={() => handleView(name, bgcolor, navigator)}>
      <Text style={styles('').todoListTitle}>{name}</Text>
      <Text style={styles('').todoListNumber}>1</Text>
      <Text style={styles('').todoListSubtitle}>Remaining</Text>
      <Text style={styles('').todoListNumber}>2</Text>
      <Text style={styles('').todoListSubtitle}>Completed</Text>
    </TouchableOpacity>
  );
};

export default TodoList;
