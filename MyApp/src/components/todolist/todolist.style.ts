import { StyleSheet } from "react-native";
import { color, dimension } from "../../styles";
export const styles=(props:string)=>StyleSheet.create({
  todoListTitle:{
    color: color.WHITE,
    fontSize: dimension.FONT_SIZE_L,
    fontWeight:'700',
    marginBottom:20,
    

  },
  todoListNumber:{
    color: color.WHITE,
    fontSize: dimension.FONT_SIZE_L,
    // fontWeight:'300',
    marginLeft:20


  },
  todoListSubtitle:{
    color: color.WHITE,
    fontSize: dimension.FONT_SIZE_L,
    fontWeight:'500',
    marginBottom:20,
    marginLeft:10

  },
  todolist:{
    width: 150,
    height: 220,
    borderRadius:3,
    backgroundColor:props,
    padding:20,
    marginRight:30,


  },
    


})

