import React from 'react';
import {TouchableOpacity} from 'react-native';
import {styles} from './colorPicker.style';
const ColorPicker: React.FC<any> = ({padding, color, colorset}) => (
  <TouchableOpacity
    style={styles(padding, color).colorPicker}
    onPress={() => colorset.setFieldValue('color', color)}></TouchableOpacity>
);
export default ColorPicker;
