export default{
    PRIMARY:'#CCC',
    SECONDARY:'#000000',
    BLUE:'#1AA7EC',
    BLACK:'#000000',
    WHITE:'#FFFFFF',
    GREY:'#CCC',
    RED:'red',
    SKYBLUE:'#0ceafa',
    GREEN:'#37ed6a',
    PURPLEBLUE:'#8337ed',
    PURPLE:'#a73ec9',
    PINK:'#f542d1',
    ORANGE:'#f56342',
    ORANGEYELLOW:'#f59042'

}