import font from "./fonts.style";
import color from "./colors.style";
import dimension from "./dimensions.style";
import typography from "./typography";
export {font,color,dimension,typography}
