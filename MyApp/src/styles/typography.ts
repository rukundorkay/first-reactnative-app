import {StyleSheet} from 'react-native';
import Dimensions from './dimensions.style';
import Colors from './colors.style';
import {font} from '.';

export default StyleSheet.create({
  title: {
    color: Colors.WHITE,
    fontSize: Dimensions.FONT_SIZE_XL,
    // ...font.medium,
  },
  subtitle: {
    fontFamily: 'Raleway-ExtraBold',
    color: Colors.WHITE,
    fontSize: Dimensions.FONT_SIZE_L,
  },
  body: {
    fontFamily: 'Raleway-Medium',
    color: Colors.BLACK,
    fontSize: Dimensions.FONT_SIZE_M,
  },
  caption: {
    fontFamily: 'Raleway-Medium',
    color: Colors.BLACK,
    fontSize: Dimensions.FONT_SIZE_SM,
  },
  
  label: {
    fontFamily: 'Raleway-ExtraBold',
    color: Colors.BLACK,
    fontSize: Dimensions.FONT_SIZE_SM,
  },
});
