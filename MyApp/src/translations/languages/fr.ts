export const translation = {
  appDescription: "form app",
  shipmentTitle: "Créer un envoi",
  shipmentSubTitle: "1 expéditeur sur 6",
  requiredFieldIndicator: "Indique les champs obligatoires ",
  shipper:"le transporteur",
  CompanyName:"Nom de l'entreprise",
  Location:"lieu",
  Address:"Adresse",
  back:"Retourner",
  next:"suivant"
};

export default translation;
