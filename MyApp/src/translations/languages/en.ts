export const translation = {
  appDescription: "form App",
  shipmentTitle: "Create Shipment",
  shipmentSubTitle: "1 of 6 shipper",
  requiredFieldIndicator: "Indicates Required Fields ",
  shipper:"shipper",
  CompanyName:"Company Name",
  Location:"Address",
  back:"Back",
  next:"Next"
};

export default translation;
