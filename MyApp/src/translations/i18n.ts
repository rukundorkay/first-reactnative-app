import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import TRANSLATIONS_EN from "./languages/en";
import TRANSLATIONS_FR from "./languages/fr";

export const initializeI18n = () => {
  i18n
    .use(initReactI18next)
    .init({
      compatibilityJSON: 'v3',
      lng:'fr',
      fallbackLng: 'en',
      resources: {
        en: {
          translation: TRANSLATIONS_EN,
        },
        fr: {
          translation: TRANSLATIONS_FR,
        },
       
      },
    });
};
