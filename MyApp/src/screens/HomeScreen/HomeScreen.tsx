import {NavigationProp} from '@react-navigation/native';
import React, {useState} from 'react';
import {ScrollView, Text, View, TouchableOpacity} from 'react-native';
import {styles} from './HomeScreen.styles';
import {color, typography} from '../../styles';
import Todolist from '../../components/todolist';
import {useTodoList} from '../../context/TodoList';

const HomeScreen = ({navigation}: {navigation: NavigationProp<any, any>}) => {
  const {todoList, setTodoList}: any = useTodoList();

  const TodoLists = () =>
    todoList.map((item: any, index: number) => {
      return (
        <Todolist
          key={index}
          name={item.name}
          bgcolor={item.ikureri}
          navigator={navigation}
        />
      );
    });

  return (
    <ScrollView style={styles.screen}>
      <View style={styles.AddListwrapper}>
        <View style={styles.flexer}>
          <Text style={styles.title}>Todo</Text>
          <Text style={styles.subtitle}> Lists</Text>
        </View>
        <View style={styles.AddList}>
          <TouchableOpacity
            style={styles.AddListButton}
            onPress={() => navigation.navigate('CreateTodoList')}>
            <Text style={styles.AddListButtonContent}>+</Text>
          </TouchableOpacity>
          <Text style={{color: color.SKYBLUE}}>Add List</Text>
        </View>
      </View>
      <ScrollView
        horizontal={true}
        style={styles.todolistWrapper}
        showsHorizontalScrollIndicator={false}>
        <TodoLists />
      </ScrollView>
    </ScrollView>
  );
};

export default HomeScreen;
