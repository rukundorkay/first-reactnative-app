import {StyleSheet} from 'react-native';
import { dimension,color } from '../../styles';

export const styles = StyleSheet.create({
  todolistWrapper:{
    marginLeft:30

  },
 
  AddListwrapper:{
    marginTop:120,
    marginLeft:120,
    marginBottom:50,
   
  },
  AddList:{
    marginTop:30,
    marginHorizontal:40,
   
  },
  AddListButton:{
    width: 50,
    height: 40,
    borderWidth: 1,
    paddingTop:10,
    paddingLeft:20,
    borderRadius:3,
    // textAlign:'center',
    // justifyContent: 'center',
    borderColor:color.SKYBLUE,

  },
  AddListButtonContent:{
    color:color.SKYBLUE
    
  },
  flexer:{
    flexDirection:'row',
    flex:1,
    
  },
  title: {
    color: color.BLACK,
    fontSize: dimension.FONT_SIZE_XL,
    fontWeight:'700'
   
  },
  subtitle: {
    fontFamily: 'Raleway-ExtraBold',
    color: color.SKYBLUE,
    fontSize: dimension.FONT_SIZE_XL,
  },
  
  
  screen:{
    backgroundColor:color.WHITE
  }
  
});
