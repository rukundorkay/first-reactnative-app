import { StyleSheet } from 'react-native';
import { dimension, color } from '../../styles';


export const styles = StyleSheet.create({
  exit:{
    marginTop:30,
    alignItems: 'flex-end',
    marginRight:20

  },
  
  
  colorPickerWrapper: {
    marginTop:10,
    marginBottom:10,
    flexDirection:'row'

  },

  title: {
    marginLeft: 60,
    fontFamily: 'Raleway-ExtraBold',
    color: color.BLACK,
    fontSize: dimension.FONT_SIZE_XL,
    fontWeight: '900',
  },

  input: {
    marginTop: 20,
    borderWidth: 0.3,
    borderRadius: 3,
    width: 300,
    height: 40
  },
  screen: {
    backgroundColor: color.WHITE
  },
  body: {
    marginTop: 140,
    marginLeft: 40

  }

});
