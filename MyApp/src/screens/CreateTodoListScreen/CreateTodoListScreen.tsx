import {NavigationProp, Route, StackActions} from '@react-navigation/native';
import {Formik} from 'formik';
import React from 'react';
import Feather from 'react-native-vector-icons/Feather';
import {
  ScrollView,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  NavigatorIOS,
} from 'react-native';
import Colorpicker from '../../components/colorpicker';
import {color, dimension} from '../../styles';
import {styles} from './CreateTodoListScreen.styles';
import {MyFormValues} from '../../interfaces/formik.interface';
import ButtonView from '../../components/button/button';
import formSchema from '../../validators/form.validators';
import {useTodoList} from '../../context/TodoList';

const CreateTodoListScreen = ({
  navigation,
}: {
  navigation: NavigationProp<any, any>;
}) => {
  const formikValue: MyFormValues = {
    name: '',
    color: color.BLUE,
  };
  const {todoList, setTodoList}: any = useTodoList();
  const popAction = StackActions.pop(1);
  return (
    <ScrollView style={styles.screen}>
      <TouchableOpacity
        style={styles.exit}
        onPress={() => navigation.dispatch(popAction)}>
        <Feather name="x" size={dimension.FONT_SIZE_XL} color={color.BLACK} />
      </TouchableOpacity>

      <View style={styles.body}>
        <Formik
          initialValues={formikValue}
          validationSchema={formSchema}
          onSubmit={values => {
            setTodoList([
              ...todoList,
              {
                name: values.name,
                ikureri: values.color,
                todos: [],
              },
            ]);
            navigation.dispatch(popAction);
          }}>
          {prop => (
            <View>
              <Text style={styles.title}>Create Todo List </Text>
              <TextInput
                placeholder="Last Name?"
                style={styles.input}
                onChangeText={prop.handleChange('name')}
              />
              {prop.errors.name && prop.touched.name ? (
                <View>
                  <Text>{prop.errors.name}</Text>
                </View>
              ) : null}
              <View style={styles.colorPickerWrapper}>
                <Colorpicker padding={0} color={color.GREEN} colorset={prop} />
                <Colorpicker padding={14} color={color.BLUE} colorset={prop} />
                <Colorpicker
                  padding={14}
                  color={color.PURPLEBLUE}
                  colorset={prop}
                />
                <Colorpicker
                  padding={14}
                  color={color.PURPLE}
                  colorset={prop}
                />
                <Colorpicker padding={14} color={color.PINK} colorset={prop} />
                <Colorpicker
                  padding={14}
                  color={color.ORANGE}
                  colorset={prop}
                />
                <Colorpicker
                  padding={14}
                  color={color.ORANGEYELLOW}
                  colorset={prop}
                />
              </View>
              <ButtonView color={prop.values.color} submit={prop} />
            </View>
          )}
        </Formik>
      </View>
    </ScrollView>
  );
};

export default CreateTodoListScreen;
