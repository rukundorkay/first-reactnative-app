import { StyleSheet } from 'react-native';
import { dimension, color } from '../../styles';
import colorsStyle from '../../styles/colors.style';


export const styles=(currentColor:string)=> StyleSheet.create({
  checkboxTitle:{
    marginTop:4,
    fontWeight: '900',
    fontFamily: 'Raleway-ExtraBold',
    color: color.BLACK,

  },
  addButtonContext:{
    color:colorsStyle.WHITE
  },
  input: {
    borderWidth: 0.3,
    borderRadius: 3,
    width: 250,
    height: 40,
    marginRight:10
  },
  addbuttonWrapper:{
   flexDirection:'row',
   marginTop:230,
   marginLeft:30
   
  },
  addButton:{
    backgroundColor:color.PURPLE,
    width: 50,
    height: 40,
    paddingTop:10,
    paddingLeft:20,
    borderRadius:3,
    

  },
  checkboxViewWrapper:{
    marginTop:20,
    marginLeft:40

  },

   checkboxView:{
    flexDirection: 'row',
    marginTop:20

   },
  hrWrapper:{
    marginLeft:70,
    marginTop:10
  },

  hr:{
    width:290,
    height:2,
    backgroundColor:currentColor
  },

  headerText: {
    marginRight:50,
    fontFamily: 'Raleway-ExtraBold',
    color: color.BLACK,
    fontSize: dimension.FONT_SIZE_XL,
    fontWeight: '900',
  },
  header: {
    flexDirection: 'row',
    marginLeft:70,
    marginTop:10
    
  },

  

});
