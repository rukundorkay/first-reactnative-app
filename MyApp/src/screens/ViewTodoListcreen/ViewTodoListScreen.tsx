import {NavigationProp, Route, StackActions} from '@react-navigation/native';
import {Formik} from 'formik';
import React, {useState} from 'react';
import Feather from 'react-native-vector-icons/Feather';
import {useCurrentColor} from '../../context/CurrentColor';
import {
  ScrollView,
  Text,
  View,
  TextInput,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {color, dimension} from '../../styles';
import {styles} from './ViewTodoListScreen.styles';
import CheckBox from '@react-native-community/checkbox';
import {useTodoList} from '../../context/TodoList';
import {addTaskFormValue} from '../../interfaces/formik.interface';

const CreateTodoListScreen = ({
  navigation,
}: {
  navigation: NavigationProp<any, any>;
}) => {
  const popAction = StackActions.pop(1);
  const {currentColor, setCurrentColor}: any = useCurrentColor();
  const {todoList, setTodoList}: any = useTodoList();
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const todoItems: any = todoList.find(
    (todoItem: any) => todoItem.name === currentColor.name,
  ).todos;
  const formikValue: addTaskFormValue = {
    name: '',
  };

  return (
    <Formik
      initialValues={formikValue}
      onSubmit={(values, {resetForm}) => {
        todoList.map((x: any) =>
          x.name === currentColor.name
            ? x.todos.push(values.name)
            : x.todos.push(),
        );
        resetForm();
      }}>
      {prop => (
        <View>
          <View style={styles(currentColor.color).header}>
            <Text style={styles('').headerText}>{currentColor.name}</Text>
            <TouchableOpacity onPress={() => navigation.dispatch(popAction)}>
              <Feather
                name="x"
                size={dimension.FONT_SIZE_XL}
                color={color.BLACK}
              />
            </TouchableOpacity>
          </View>
          <View style={styles('').hrWrapper}>
            <Text>0 of {todoItems.length} task</Text>
            <View style={styles(currentColor.color).hr}></View>
          </View>
          <View>
            <FlatList
              nestedScrollEnabled
              style={styles('').checkboxViewWrapper}
              data={todoItems}
              renderItem={({item}) => (
                <View style={styles('').checkboxView}>
                  <CheckBox
                    disabled={false}
                    value={toggleCheckBox}
                    onValueChange={newValue => setToggleCheckBox(newValue)}
                  />
                  <Text style={styles('').checkboxTitle}>{item}</Text>
                </View>
              )}
            />
            <View style={styles('').addbuttonWrapper}>
              <TextInput
                style={styles('').input}
                placeholder="add new item"
                onChangeText={prop.handleChange('name')}
              />
              <TouchableOpacity
                style={styles('').addButton}
                onPress={prop.handleSubmit}>
                <Text style={styles('').addButtonContext}>+</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}
    </Formik>
  );
};

export default CreateTodoListScreen;
