import React from 'react';
import {initializeI18n} from './src/translations/i18n';
import AppNavigator from './src/navigators/AppNavigator';
import CurrentColorProvider from './src/context/CurrentColor/CurrentColorProvider';
import TodoListProvider from './src/context/TodoList/TodoListProvider';

const App = () => {
  initializeI18n();
  return (
    <TodoListProvider>
      <CurrentColorProvider>
        <AppNavigator />
      </CurrentColorProvider>
    </TodoListProvider>
  );
};

export default App;
